# ignore FutureWarnings
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
# disable tensorflow CPU warning
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import numpy as np
import pandas as pd

from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.utils import np_utils
from keras import optimizers

import time

#set seeds
from numpy.random import seed
seed(955)
from tensorflow import set_random_seed
set_random_seed(2)
#splitSeed for train_test_split
splitSeed=6544

print('Feedforward Neural Network')

# read data in from csv
data = pd.read_csv('creditcard.csv', header=0)

# split data and target variable
X = data.iloc[:, 1:30].values
y = data.iloc[:, -1].values

# scale data
X_scaler = StandardScaler()
X = X_scaler.fit_transform(X)

# sequential feed forward network
# two hidden layers and sigmoid output activation for binary classification
model = Sequential()

# layer 1
model.add(Dense(58, input_shape=(29,)))
model.add(Activation('relu'))                   
model.add(Dropout(0.2)) 

# layer 2
model.add(Dense(29))
model.add(Activation('relu'))
model.add(Dropout(0.2))

# output layer
model.add(Dense(1))
model.add(Activation('sigmoid'))

# compile the model
model.compile(loss='binary_crossentropy', 
              optimizer='adadelta',
              metrics=['accuracy'])


# split data into training validation and testing
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.3,random_state=splitSeed)

X_train, X_validate, y_train, y_validate=train_test_split(X_train,y_train,test_size=0.3,random_state=splitSeed)

# train the model 
print('Model Fitting')
modelFitStart=(time.clock())
model.fit(X_train, y_train,
          batch_size=2048, epochs=300,verbose=0,
          validation_data=(X_validate, y_validate))
modelFitEnd=(time.clock())
print('Model Fit Time')
print(modelFitEnd-modelFitStart)

print('Model Predicting')
modelPredictStart=(time.clock())
yp=np.round(model.predict(X_test))
modelPredictEnd=(time.clock())
print('Model Prediction Time')
print(modelPredictEnd-modelPredictStart)

# load metrics
from sklearn.metrics import accuracy_score as acc
from sklearn.metrics import confusion_matrix
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
from sklearn.metrics import matthews_corrcoef as mmc

print('Accuracy: ')
print(acc(y_test,yp))
print('\nRecall: ')
print(recall_score(y_test,yp))
print('\nPrecision')
print(precision_score(y_test,yp))
print('\nMatthews Correlation Coefficient: ')
print(mmc(y_test,yp))
print('\nConfusion Matrix')
print(confusion_matrix(y_test,yp))

# clears tensorflow session, without will sometimes throw exception
from keras import backend as K

K.clear_session()
