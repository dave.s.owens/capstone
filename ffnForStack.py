# ignore FutureWarnings
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
# disable tensorflow CPU warning
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

import numpy as np
import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.utils import np_utils
from keras import optimizers

import time

#set seeds
from numpy.random import seed
seed(955)
from tensorflow import set_random_seed
set_random_seed(2)
#splitSeed for train_test_split
splitSeed=553399

# csvs are populated in stackPrep.R this ensures all components of the 
# stack use the same training data to prevent leakage
individualTrain=pd.read_csv('rIndTrain.csv',header=0)
stackTrain=pd.read_csv('rStackTrain.csv',header=0)
test=pd.read_csv('rTest.csv',header=0)

# split data and target variable
# FFN training
indX = individualTrain.iloc[:, 1:30].values
indy = individualTrain.Class
# stack training
stackX = stackTrain.iloc[:, 1:30].values 
stacky = stackTrain.Class
# stack testing
testX = test.iloc[:, 1:30].values
testy = test.Class

# whiten data
XInd_scaler = StandardScaler()
indX = XInd_scaler.fit_transform(indX)

XStack_scaler = StandardScaler()
stackX = XStack_scaler.fit_transform(stackX)

XTest_scaler = StandardScaler()
testX = XTest_scaler.fit_transform(testX)

# sequential feed forward network
# two hidden layers and sigmoid output activation for binary classification
model = Sequential()

# layer 1
model.add(Dense(58, input_shape=(29,)))
model.add(Activation('relu'))                   
model.add(Dropout(0.2)) 

# layer 2
model.add(Dense(29))
model.add(Activation('relu'))
model.add(Dropout(0.2))

# output layer
model.add(Dense(1))
model.add(Activation('sigmoid'))

# compile the model
model.compile(loss='binary_crossentropy', 
              optimizer='adadelta',
              metrics=['accuracy'])


from sklearn.model_selection import train_test_split

X_train,X_validate,y_train,y_validate=train_test_split(indX, indy, test_size = 0.3,random_state=splitSeed)

# train the model 
print('ffn')
modelFitStart=(time.clock())
model.fit(X_train, y_train,
          batch_size=2048, epochs=300,verbose=0,
          validation_data=(X_validate, y_validate))
modelFitEnd=(time.clock())
print('Model Fit Time')
print(modelFitEnd-modelFitStart)

modelPredictStart=(time.clock())
ypStack=np.round(model.predict(stackX))
modelPredictEnd=(time.clock())
print('Model Prediction Time')
print(modelPredictEnd-modelPredictStart)

ypStack=pd.DataFrame(ypStack)
pd.DataFrame.to_csv(ypStack,'ffnPreds.csv')

ypTest=np.round(model.predict(testX))
ypTest=pd.DataFrame(ypTest)
pd.DataFrame.to_csv(ypTest,'ffnTestPreds.csv')

# clears tensorflow session, without will sometimes throw exception
from keras import backend as K

K.clear_session()


